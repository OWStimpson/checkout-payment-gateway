# Checkout Payment Gateway  

### Running the project    
This solution has multiple start up projects, to enable right click on the to level solution and choose properties. On the solution properties dialog choose *Multiple Start-up Projects* and change the action to start for *checkout-payment-gateway-api* and *simulated-bank-api*.  

![Screenshot](solution-items/startup.png)  

Running the project should now load the swagger pages for both api's. The simulated bank can just be left running in the background. Use the checkout-payment-api swagger page to test the application.  



### GET /api/payments/\{id\}
Retrieves the details of a single payment and returns a json object wit hthe payment details. 

Example response:
~~~
{
  "id": 7,
  "cardNumber": "************1234",
  "cardHolderName": "Mr O Stimpson",
  "expiryDate": "11/11",
  "cvv": 999,
  "amount": 100,
  "currency": "NZD",
  "reference": "Some Ref",
  "status": "Approved",
  "uniqueBankReference": "4a288904-0ccd-4f91-ab33-9ff927cc1782"
}
~~~

### POST /api/payments  
Takes in a new payment request, validates the payment with the simulated bank and stores the result in the api's database. The result of the request will be returned from the api as a json response. 

Example request body:
~~~
{
  "cardNumber": "1234123412341234",
  "cardHolderName": "Mr O Stimpson",
  "expiryDate": "11/11",
  "cvv": "999",
  "amount": 100.00,
  "currency": "NZD",
  "reference": "Some Ref"
}
~~~

Example response:
~~~
{
  "id": 7,
  "status": "Approved"
}
~~~

### Assumptions 

- I have not specifically validated the format of the request between the checkout api and the bank. The bank simply retuns a random response for the data passed to it. 
- I have not taken into account idempotence of the requests, so the same transition can be posted over and over. This be facilitated with some unique client id in the request header, to allow failed requests to be retried safely. 


### If I had more time  

 - Extend the unit testing to cover all the validation.
 - Add integration tests to the project.
 - Add health checks.
 - Containerise the solution to use Docker compose insstead of multiple start up projects.
 - I added application logging and data storage from the optional deliverables, but given more time I would work through some more of these.