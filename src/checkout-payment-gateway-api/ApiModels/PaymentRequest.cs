﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.ApiModels
{
    /// <summary>
    /// Defines the structure of a request to make a payment
    /// </summary>
    public class PaymentRequest
    {
        /// <summary>
        /// Long card number
        /// </summary>
        [Required(ErrorMessage = "Card number cannot be empty.")]
        [MinLength(16, ErrorMessage = "Card number must be 16 characters.")]
        [MaxLength(16, ErrorMessage = "Card number must be 16 characters.")]
        public string CardNumber { get; set; }

        /// <summary>
        /// Card holders full name
        /// </summary>
        [Required(ErrorMessage = "Card holder name cannot be empty.")]
        [MaxLength(200)]
        public string CardHolderName { get; set; }

        /// <summary>
        /// Four digit expiry date MM/YY
        /// </summary>
        [Required(ErrorMessage = "Expiry date cannot be empty.")]
        [MaxLength(5)]
        [MinLength(5)]
        [RegularExpression("^(0[1-9]|1[0-2])//?([0-9]{2})$", ErrorMessage = "Expiry date must be in the format MM/YY")]
        public string ExpiryDate { get; set; }

        /// <summary>
        /// Three digit CVV code 
        /// </summary>
        [Required(ErrorMessage = "CVV cannot be empty.")]
        [MinLength(3, ErrorMessage = "CVV must be 3 numbers.")]
        [MaxLength(3, ErrorMessage = "CVV must be 3 numbers.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "CVV must be numeric")]
        public string Cvv { get; set; }

        /// <summary>
        /// Amount of the payment
        /// </summary>
        [Required(ErrorMessage = "Amount cannot be empty.")]
        [Range(0, 9999999999999999.99, ErrorMessage = "Amount must be greater than zero.")]
        public decimal Amount { get; set; }

        /// <summary>
        /// The payment currency
        /// </summary>
        [Required(ErrorMessage = "Currency cannot be empty.")]
        [MaxLength(3)]
        public string Currency { get; set; }

        /// <summary>
        /// Clients payment reference
        /// </summary>
        [MaxLength(100)]
        public string Reference { get; set; }
    }
}
