﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.ApiModels
{
    /// <summary>
    /// Defines the structure of a request to make a payment
    /// </summary>
    public class PaymentResponse
    {
        /// <summary>
        /// ID of the payment
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Long card number
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
        /// Card holders full name
        /// </summary>
        public string CardHolderName { get; set; }
        /// <summary>
        /// Four digit expiry date MM/YY
        /// </summary>
        public string ExpiryDate { get; set; }
        /// <summary>
        /// Three digit CVV code 
        /// </summary>
        public short Cvv { get; set; }
        /// <summary>
        /// Amount of the payment
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// The payment currency
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Clients payment reference
        /// </summary>
        public string Reference { get; set; }
        /// <summary>
        /// Status of the payment
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Banks reference for the payment
        /// </summary>
        public Guid UniqueBankReference { get; set; }
    }
}
