﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.ApiModels
{
    public class PaymentStatusResponse
    {
        /// <summary>
        /// Unique id of the payment
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Status of the payment
        /// </summary>
        public string Status { get; set; }
    }
}
