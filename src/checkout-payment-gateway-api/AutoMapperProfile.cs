﻿using AutoMapper;
using checkout_payment_gateway_api.ApiModels;
using checkout_payment_gateway_service.Entity;
using checkout_payment_gateway_service.ServiceModels;

namespace checkout_payment_gateway_api.Mapping
{
    /// <summary>
    /// Setup up class for mapping profiles.
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Profiles.
        /// </summary>
        public AutoMapperProfile()
        {
            // Controller Mappings
            CreateMap<PaymentRequest, Payment>();
            CreateMap<Payment, PaymentStatusResponse>();
            CreateMap<Payment, PaymentResponse>();

            // Service Mappings
            CreateMap<Payment, BankPaymentRequest>();
        }
    }
}
