﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace checkout_payment_gateway_api.Controllers
{
    /// <summary>
    /// Base class for all controllers.
    /// </summary>
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// Gets the CorrelationId from the http request.
        /// </summary>
        public string CorrelationId
        {
            get => HttpContext.Items["X-Correlation-ID"].ToString();
        }
    }
}


