﻿using AutoMapper;
using checkout_payment_gateway_api.ApiModels;
using checkout_payment_gateway_api.Helpers;
using checkout_payment_gateway_service.Entity;
using checkout_payment_gateway_service.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.Controllers
{
    /// <summary>
    /// Manages making and retrieving payments.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : BaseController
    {
        private const string _loggingPrefix = nameof(PaymentsController);
        private readonly ILogger<PaymentsController> _logger;
        private readonly IMapper _mapper;
        private readonly IPaymentService _paymentService;
        private readonly ICurrencyValidator _currencyValidator;

        public PaymentsController(ILogger<PaymentsController> logger, 
                                  IMapper mapper, 
                                  IPaymentService paymentService, 
                                  ICurrencyValidator currencyValidator)
        {
            _logger = logger;
            _mapper = mapper;
            _paymentService = paymentService;
            _currencyValidator = currencyValidator;
        }

        /// <summary>
        /// Retrieve the details of a single payment.
        /// </summary>
        /// <param name="id">ID of the payment to be retrieved</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Get(int id)
        {
            const string localLoggerPrefix = _loggingPrefix + ":" + nameof(Get) + ":";
            try
            {
                var result = await _paymentService.GetPayment(id);

                if (result == null)
                {
                    return StatusCode(StatusCodes.Status404NotFound);
                }

                return StatusCode(StatusCodes.Status200OK, _mapper.Map<PaymentResponse>(result));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{localLoggerPrefix} Exception.");
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        /// <summary>
        /// Request a new payment to be made
        /// </summary>
        /// <param name="paymentRequest">Details of the payment to be requested</param>
        /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody] PaymentRequest paymentRequest)
        {
            const string localLoggerPrefix = _loggingPrefix + ":" + nameof(Post) + ":";
            try
            {
                // Ensure the currency passed in is a valid ISO code
                if (!_currencyValidator.ValidIsoCurrency(paymentRequest.Currency.ToUpper()))
                {
                    ModelState.AddModelError(nameof(paymentRequest.Currency), "Currency must be a valid ISO code.");
                }

                if (ModelState.IsValid)
                {
                    var payment = _mapper.Map<Payment>(paymentRequest);
                    var result = await _paymentService.PaymentRequest(payment);
                    return StatusCode(StatusCodes.Status201Created, _mapper.Map<PaymentStatusResponse>(result));
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{localLoggerPrefix} Exception.");
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}

