﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.Helpers
{
    /// <summary>
    /// Validates ISO currency codes
    /// </summary>
    public class CurrencyValidator : ICurrencyValidator
    {
        /// <summary>
        /// Gets the list of ISO currency codes
        /// </summary>
        private static IEnumerable<string> GetIsoCurrencyCodes()
        {
            var countries = new List<RegionInfo>();
            foreach (var culture in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                var country = new RegionInfo(culture.LCID);
                if (countries.Where(p => p.Name == country.Name).Count() == 0)
                {
                    countries.Add(country);
                }
            }
            return countries.Select(c => c.ISOCurrencySymbol).Distinct().ToList();
        }

        /// <summary>
        /// Validate that given string is an ISO currency code
        /// </summary>
        /// <param name="currecyCode"></param>
        /// <returns>bool</returns>
        public bool ValidIsoCurrency(string currecyCode) => GetIsoCurrencyCodes().Contains(currecyCode);
    }
}
