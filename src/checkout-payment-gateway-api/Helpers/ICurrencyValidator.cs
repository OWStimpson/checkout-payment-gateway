﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace checkout_payment_gateway_api.Helpers
{ 
    /// <summary>
    /// Validates ISO currency codes
    /// </summary>
    public interface ICurrencyValidator
    {
        /// <summary>
        /// Validate that given string is an ISO currency code
        /// </summary>
        /// <param name="currecyCode"></param>
        /// <returns>bool</returns>
        bool ValidIsoCurrency(string currecyCode);
    }
}
