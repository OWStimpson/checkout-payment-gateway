using AutoMapper;
using checkout_payment_gateway_api.Helpers;
using checkout_payment_gateway_api.Middleware;
using checkout_payment_gateway_service.Data;
using checkout_payment_gateway_service.Helpers;
using checkout_payment_gateway_service.Service;
using checkout_payment_gateway_service.ServiceModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace checkout_payment_gateway_api
{
    public class Startup
	{
		private readonly IWebHostEnvironment _env;

		public Startup(IConfiguration config, IWebHostEnvironment env)
		{
			Configuration = config;
			_env = env;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<CookiePolicyOptions>(options =>
			{
				options.CheckConsentNeeded = context => true;
				options.MinimumSameSitePolicy = SameSiteMode.None;
			});

			// configure strongly typed settings objects
			var appSettingsSection = Configuration.GetSection("AppSettings");
			services.Configure<AppSettings>(appSettingsSection);
			var appSettings = appSettingsSection.Get<AppSettings>();

			string connectionString = Configuration.GetConnectionString("SqliteConnection");

			services.AddDbContext<AppDbContext>(options => options.UseSqlite(connectionString));

			services.AddControllers().AddNewtonsoftJson();

			services.AddRouting(options => options.LowercaseUrls = true);

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "Checkout Payment Gateway API",
					Version = "v1",
					Description = "API to allow the requesting and retrieval of payments."
				});
				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);
			});

			services.AddAutoMapper(typeof(Startup));

			services.AddScoped<IRepository, Repository>();
			services.AddScoped<IPaymentService, PaymentService>();
			services.AddScoped<ICurrencyValidator, CurrencyValidator>();
			services.AddScoped<ICardMasker, CardMasker>();

			services.AddHttpClient<IBankHttpClient, BankHttpClient>(client =>
			{
				client.BaseAddress = new Uri(appSettings.BankApiUrl);
			});
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.EnvironmentName == "Development")
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}
			app.UseRouting();

			app.UseHttpsRedirection();

            // For test purposes I have just pointed the default log target at the console. 
            app.UseMiddleware<LoggingMiddleware>();

			app.UseStaticFiles();

			app.UseCookiePolicy();

			app.UseSwagger();

			app.UseSwaggerUI(c => { 
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Checkout Payment Gateway API");
				c.RoutePrefix = string.Empty;  // Set Swagger UI at apps root
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapDefaultControllerRoute();
			});
		}
	}
}
