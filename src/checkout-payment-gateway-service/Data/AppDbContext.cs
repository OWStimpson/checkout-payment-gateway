﻿using checkout_payment_gateway_service.Entity;
using Microsoft.EntityFrameworkCore;

namespace checkout_payment_gateway_service.Data
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Payment> Payment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Payments
            modelBuilder.Entity<Payment>().HasKey(p => p.Id);
            base.OnModelCreating(modelBuilder);
        }
    }
}