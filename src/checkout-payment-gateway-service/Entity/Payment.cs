﻿using System;

namespace checkout_payment_gateway_service.Entity
{
    public class Payment : BaseEntity
    {
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string ExpiryDate { get; set; }
        public short Cvv { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Reference { get; set; }
        public string Status { get; set; }
        public Guid UniqueBankReference { get; set; }
    }
}
