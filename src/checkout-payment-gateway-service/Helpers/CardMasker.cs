﻿using System;
using System.Collections.Generic;
using System.Text;

namespace checkout_payment_gateway_service.Helpers
{
    public class CardMasker : ICardMasker
    {
        public string Mask(string cardNumber)
        {
            if (cardNumber.Length < 16)
            {
                return "****************";
            }
            return $"************{cardNumber.Substring(12, 4)}";
        }
    }
}
