﻿using System;
using System.Collections.Generic;
using System.Text;

namespace checkout_payment_gateway_service.Helpers
{
	public interface ICardMasker
	{
		public string Mask(string cardNumber);
	}
}
