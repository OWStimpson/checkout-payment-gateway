﻿using checkout_payment_gateway_service.ServiceModels;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace checkout_payment_gateway_service.Service
{
    /// <summary>
    /// Http Client to support calls to the simulated bank.
    /// </summary>
    public class BankHttpClient : IBankHttpClient
    {
        private const string _loggingPrefix = nameof(BankHttpClient);
        private readonly ILogger<BankHttpClient> _logger;
        private readonly HttpClient _httpClient;

        private string _localLoggerPrefix;

        public BankHttpClient(ILogger<BankHttpClient> logger, HttpClient httpClient)
        {
            _logger = logger;
            _httpClient = httpClient;
        }

        /// <summary>
        /// Build a post request to instruct the bank to make a payment.
        /// </summary>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        public async Task<BankPaymentResponse> PaymentRequest(BankPaymentRequest paymentRequest)
        {
            _localLoggerPrefix = _loggingPrefix + "." + nameof(PaymentRequest);
            var content = JsonConvert.SerializeObject(paymentRequest);
            _logger.LogInformation($"{_localLoggerPrefix}.starting call to bank - paymentRequest:{content}");
            var httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            var reqUrl = "api/bank/payments";

            try
            {
                var result = await _httpClient.PostAsync($"{reqUrl}", httpContent);

                if (result.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    throw new Exception();
                }

                var response = await result.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<BankPaymentResponse>(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{_localLoggerPrefix}.exception - paymentRequest:{content}");
                throw ex;
            }
            finally
            {
                _logger.LogInformation($"{_localLoggerPrefix}.completed - paymentRequest:{content}");
            }
        }
    }
}
