﻿using checkout_payment_gateway_service.ServiceModels;
using System.Threading.Tasks;

namespace checkout_payment_gateway_service.Service
{
    public interface IBankHttpClient
    {
        Task<BankPaymentResponse> PaymentRequest(BankPaymentRequest paymentRequest);
    }
}
