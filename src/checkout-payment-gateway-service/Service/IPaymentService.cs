﻿using checkout_payment_gateway_service.Entity;
using System.Threading.Tasks;

namespace checkout_payment_gateway_service.Service
{
    public interface IPaymentService
    {
        Task<Payment> GetPayment(int id);
        Task<Payment> PaymentRequest(Payment payment);
    }
}
