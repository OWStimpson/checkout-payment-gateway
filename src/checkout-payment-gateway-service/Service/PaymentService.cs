﻿using AutoMapper;
using checkout_payment_gateway_service.Data;
using checkout_payment_gateway_service.Entity;
using checkout_payment_gateway_service.Helpers;
using checkout_payment_gateway_service.ServiceModels;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace checkout_payment_gateway_service.Service
{
    public class PaymentService : IPaymentService
    {
        private const string _loggingPrefix = nameof(PaymentService);
        private readonly ILogger<PaymentService> _logger;
        private readonly IMapper _mapper;
        private readonly IRepository _repository;
        private readonly IBankHttpClient _bankHttpClient;
        private readonly ICardMasker _cardMasker;

        public PaymentService(ILogger<PaymentService> logger, 
                              IMapper mapper, 
                              IRepository repository, 
                              IBankHttpClient bankHttpClient,
                              ICardMasker cardMasker)
        {
            _logger = logger;
            _mapper = mapper;
            _repository = repository;
            _bankHttpClient = bankHttpClient;
            _cardMasker = cardMasker;
        }

        public async Task<Payment> GetPayment(int id)
        {
            const string localLoggerPrefix = _loggingPrefix + ":" + nameof(GetPayment) + ":";
            try
            {
                var result = await _repository.GetByIdAsync<Payment>(id);
                if (result != null)
                {
                    // Ensure the returned card number is obscured
                    result.CardNumber = _cardMasker.Mask(result.CardNumber);
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{localLoggerPrefix} Exception.");
                throw;
            }
        }

        public async Task<Payment> PaymentRequest(Payment payment)
        {
            const string localLoggerPrefix = _loggingPrefix + ":" + nameof(PaymentRequest) + ":";
            try
            {
                var bankPaymentRequest = _mapper.Map<BankPaymentRequest>(payment);
                var result = await _bankHttpClient.PaymentRequest(bankPaymentRequest);
                payment.UniqueBankReference = result.UniqueBankReference;
                payment.Status = result.PaymentStatus;
                return await _repository.AddAsync(payment);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{localLoggerPrefix} Exception.");
                throw;
            }
        }
    }
}
