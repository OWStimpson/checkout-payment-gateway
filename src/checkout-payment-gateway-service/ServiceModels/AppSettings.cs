﻿namespace checkout_payment_gateway_service.ServiceModels
{
    /// <summary>
    /// Virtual settings object mapping to physical appsettings.json.
    /// </summary>
    public class AppSettings
    {
        public string BankApiUrl { get; set; }
    }
}
