﻿using System;

namespace checkout_payment_gateway_service.ServiceModels
{
    /// <summary>
    /// Response to a payment request
    /// </summary>
    public class BankPaymentResponse
    {

        /// <summary>
        /// The banks unique reference for the payment request
        /// </summary>
        public Guid UniqueBankReference { get; set; }
        /// <summary>
        /// The status of the payment request
        /// </summary>
        public string PaymentStatus { get; set; }
    }
}
