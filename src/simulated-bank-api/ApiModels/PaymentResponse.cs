﻿using simulated_bank_api.Common;
using System;

namespace simulated_bank_api.ApiModels
{
    /// <summary>
    /// Response to a payment request
    /// </summary>
    public class PaymentResponse
    {
        public PaymentResponse()
        {
            UniqueBankReference = Guid.NewGuid();        
        }
        /// <summary>
        /// The banks unique reference for the payment request
        /// </summary>
        public Guid UniqueBankReference { get; set; }
        /// <summary>
        /// The status of the payment request
        /// </summary>
        public PaymentStatus PaymentStatus { get; set; }
    }
}
