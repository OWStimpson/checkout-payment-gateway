﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simulated_bank_api.Common
{
    /// <summary>
    /// Defines the different statuses that can be returned from a payment request
    /// </summary>
    public enum PaymentStatus
    {
        /// <summary>
        /// The payment was sucessful
        /// </summary>
        Approved,
        /// <summary>
        /// The payment was declined by the bank
        /// </summary>
        Declined,
        /// <summary>
        /// There was a technical issue processing the payment
        /// </summary>
        Failed
    }
}
