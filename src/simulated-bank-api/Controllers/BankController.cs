﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using simulated_bank_api.ApiModels;
using simulated_bank_api.Factory;
using System;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace simulated_bank_api.Controllers
{
    /// <summary>
    /// Controller to simulate the response from the bank
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : BaseController
    {
        private const string _loggingPrefix = nameof(BankController);
        private readonly ILogger<BankController> _logger;
        private readonly IRandomResponseFactory _randomResponseFactory;

        public BankController(ILogger<BankController> logger, IRandomResponseFactory randomResponseFactory)
        {
            _logger = logger;
            _randomResponseFactory = randomResponseFactory;
        }

        /// <summary>
        /// Request a new payment to be made
        /// </summary>
        /// <param name="paymentRequest">Details of the payment to be requested</param>
        /// <returns></returns>
        [HttpPost("payments")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody] PaymentRequest paymentRequest)
        {
            const string localLoggerPrefix = _loggingPrefix + ":" + nameof(Post) + ":";
            try
            {
                // In the real world this request would get validated etc. For this fake bank the 
                // controller will just return a random response no matter what is passed to it.
                return StatusCode(StatusCodes.Status201Created, _randomResponseFactory.Response());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"{localLoggerPrefix} Exception.");
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}

