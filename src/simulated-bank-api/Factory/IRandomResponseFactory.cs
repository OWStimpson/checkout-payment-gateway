﻿using simulated_bank_api.ApiModels;

namespace simulated_bank_api.Factory
{
    public interface IRandomResponseFactory
    {
        PaymentResponse Response();
    }
}
