﻿using simulated_bank_api.ApiModels;
using simulated_bank_api.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simulated_bank_api.Factory
{
    /// <summary>
    /// Factory to return a random response.
    /// </summary>
    public class RandomResponseFactory : ResponseFactory, IRandomResponseFactory
    {
        /// <summary>
        /// Picks a random number between 1 and 10 and returns declined or failed for 
        /// 1 and 2, otherwise return as approved. Simulates some level of errors, while 
        /// approving most payments.
        /// </summary>
        /// <returns>PaymentResponse</returns>
        public override PaymentResponse Response()
        {
            var random = new Random().Next(1, 10);
            switch (random)
            {
                case 1:
                    return new PaymentResponse() { PaymentStatus = PaymentStatus.Declined };
                case 2:
                    return new PaymentResponse() { PaymentStatus = PaymentStatus.Failed };
                default:
                    return new PaymentResponse() { PaymentStatus = PaymentStatus.Approved };
            }
        }
    }
}

