﻿using simulated_bank_api.ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace simulated_bank_api.Factory
{
    public abstract class ResponseFactory
    {
        public abstract PaymentResponse Response();
    }
}