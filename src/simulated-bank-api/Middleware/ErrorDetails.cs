﻿using System.Text.Json;

namespace simulated_bank_api.Middleware
{
    /// <summary>
    /// Defines the error logged by the middleware.
    /// </summary>
    public class ErrorDetails
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
