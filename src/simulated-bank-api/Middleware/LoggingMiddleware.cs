﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.IO;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace simulated_bank_api.Middleware
{
    /// <summary>
    /// Middleware to automatically log all HTTP requests to the API
    /// </summary>
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggingMiddleware> _logger;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;

        public LoggingMiddleware(ILogger<LoggingMiddleware> logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }

        public async Task Invoke(HttpContext context)
        {
            var correlationId = context.Request.Headers["X-Correlation-ID"];
            if (correlationId.Count == 0)
            {
                correlationId = Guid.NewGuid().ToString();
                context.Request.Headers.Add("X-Correlation-ID", correlationId);
            }

            context.Items["CorrelationId"] = correlationId;
            context.Items["RequestId"] = Guid.NewGuid().ToString();

            context.Response.OnStarting(() =>
            {
                context.Response.Headers.Add("X-Correlation-ID", correlationId);
                return Task.CompletedTask;
            });

            context.Request.EnableBuffering();

            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);
            _logger.LogInformation($"Http Request Information - " +
                                   $"Schema:{context.Request.Scheme} " +
                                   $"Host: {context.Request.Host} " +
                                   $"Path: {context.Request.Path} " +
                                   $"QueryString: {context.Request.QueryString} " +
                                   $"Request Body: {ReadStreamInChunks(requestStream)}");

            context.Request.Body.Position = 0;

            var sw = Stopwatch.StartNew();

            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                GetControllerInfo(out var controller, out var action, out var userId);

                _logger.LogError($"{controller}.{action}.User:{userId} Error: {ex}");
                await HandleExceptionAsync(context, ex);
            }
            finally
            {
                GetControllerInfo(out var controller, out var action, out var userId);

                _logger.LogInformation($"{controller}.{action}.User:{userId} completed - duration:{sw.Elapsed.TotalMilliseconds} ms with status code: {context.Response.StatusCode}");
            }

            void GetControllerInfo(out string controller, out string action, out string userId)
            {
                controller = string.Empty;
                action = string.Empty;
                userId = string.Empty;

                try
                {
                    if (context.GetRouteData() != null)
                    {
                        userId = context.User.Identity.Name;
                        if (context.GetRouteData().Values["controller"] != null)
                        {
                            controller = context.GetRouteData().Values["controller"].ToString();
                        }

                        if (context.GetRouteData().Values["action"] != null)
                        {
                            action = context.GetRouteData().Values["action"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("GetControllerInfo:Error", ex);
                }
            }
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(readChunk,
                                                   0,
                                                   readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);
            return textWriter.ToString();
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            if (exception is HttpRequestException)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }

            return context.Response.WriteAsync(new ErrorDetails
            {
                StatusCode = context.Response.StatusCode,
                Message = exception.Message
            }.ToString());
        }
    }
}
