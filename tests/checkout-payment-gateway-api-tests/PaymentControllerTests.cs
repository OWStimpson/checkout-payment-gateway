﻿using checkout_payment_gateway_api.ApiModels;
using checkout_payment_gateway_api.Controllers;
using checkout_payment_gateway_api.Helpers;
using checkout_payment_gateway_service.Entity;
using checkout_payment_gateway_service.Service;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace checkout_payment_gateway_api_tests
{
    public class PaymentControllerTests : TestBase
    {
        public abstract class PaymentControllerTest : TestBase
        {
            protected readonly PaymentsController _paymentController;
            protected readonly Mock<IPaymentService> _paymentsServiceMock = new Mock<IPaymentService>();
            protected readonly Mock<ICurrencyValidator> _currencyValidatorMock = new Mock<ICurrencyValidator>();
            protected readonly Mock<ILogger<PaymentsController>> _loggerMock = new Mock<ILogger<PaymentsController>>();
            protected readonly int _paymentIdExists;
            protected readonly int _paymentIdNotExists;
            protected readonly int _paymentIdException;
            protected readonly Payment _paymentBeforeSubmission;
            protected readonly Payment _paymentApproved;
            protected readonly Payment _paymentNotExists;
            protected readonly PaymentResponse _paymentResponseApproved;
            protected readonly PaymentRequest _paymentRequest;
            protected readonly PaymentRequest _paymentRequestBadCurrency;
            protected readonly PaymentRequest _paymentRequestException;
            protected readonly PaymentStatusResponse _paymentResponse;

            protected PaymentControllerTest()
            {
                _paymentController = new PaymentsController(_loggerMock.Object, 
                                                            _mapper,
                                                            _paymentsServiceMock.Object,
                                                            _currencyValidatorMock.Object);
                #region Setup

                _paymentIdExists = 1;

                _paymentIdNotExists = 2;

                _paymentIdException = 3;

                _paymentApproved = new Payment()
                {
                    Id = _paymentIdExists,
                    CardNumber = "************1234",
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Status = "Approved",
                    Reference = "Some Ref",
                    UniqueBankReference = Guid.Parse("cdfc7d95-aaad-4030-9099-a6626a926837")
                };

                _paymentBeforeSubmission = new Payment()
                {
                    Id = 0,
                    CardNumber = "1234123412341234",
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Some Ref",
                    Status = "Approved",
                    UniqueBankReference = Guid.Parse("00000000-0000-0000-0000-000000000000")
                };

                _paymentNotExists = null;

                _paymentRequest = new PaymentRequest()
                {
                    CardNumber = "1234123412341234",
                    CardHolderName = "Mr O Stimpson",
                    Cvv = "999",
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Some Ref"
                };

                _paymentRequestBadCurrency = new PaymentRequest()
                {
                    CardNumber = "1234123412341234",
                    CardHolderName = "Mr O Stimpson",
                    Cvv = "999",
                    Currency = "XXX",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Some Ref"
                };

                _paymentRequestException = new PaymentRequest()
                {
                    CardNumber = "1234123412341234",
                    CardHolderName = "Mr O Stimpson",
                    Cvv = "999",
                    Currency = "EXC",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Exception"
                };

                _paymentResponse = new PaymentStatusResponse()
                {
                    Id = _paymentIdExists,
                    Status = "Approved"
                }; 

                #endregion
                #region Mocks

                _paymentsServiceMock.Setup(x => x.GetPayment(It.Is<int>(s => s.Equals(_paymentIdExists))))
            .Returns(Task.FromResult(_paymentApproved));

                _paymentsServiceMock.Setup(x => x.GetPayment(It.Is<int>(s => s.Equals(_paymentIdNotExists))))
                    .Returns(Task.FromResult(_paymentNotExists));

                _paymentsServiceMock.Setup(x => x.GetPayment(It.Is<int>(s => s.Equals(_paymentIdException))))
                    .Throws(new Exception());

                _paymentsServiceMock.Setup(x => x.PaymentRequest(It.Is<Payment>(s => s.Id == _paymentBeforeSubmission.Id)))
                    .Returns(Task.FromResult(_paymentApproved));

                _paymentsServiceMock.Setup(x => x.PaymentRequest(It.Is<Payment>(s => s.Id == _paymentBeforeSubmission.Id)))
                    .Returns(Task.FromResult(_paymentApproved));

                _currencyValidatorMock.Setup(x => x.ValidIsoCurrency(It.Is<string>(s => s.Equals(_paymentRequest.Currency))))
                    .Returns(true);

                _currencyValidatorMock.Setup(x => x.ValidIsoCurrency(It.Is<string>(s => s.Equals(_paymentRequestBadCurrency.Currency))))
                    .Returns(false);
                _currencyValidatorMock.Setup(x => x.ValidIsoCurrency(It.Is<string>(s => s.Equals(_paymentRequestException.Currency))))
                    .Throws(new Exception()); 

                #endregion
            }
        }

        public class Get : PaymentControllerTest
        {
            [Fact]
            public async Task Correct_results_are_returned_when_get_payment_is_called()
            {
                var response = await _paymentController.Get(_paymentIdExists);
                var result = response.As<ObjectResult>().Value;

                result.Should().BeEquivalentTo(_paymentApproved);
            }

            [Fact]
            public async Task Not_exists_is_returned_when_no_payment_is_found()
            {
                var response = await _paymentController.Get(_paymentIdNotExists);
                var result = response.As<StatusCodeResult>();

                result.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]
            public async Task Returns_500_response_when_unhandled_exception_occurs()
            {
                var response = await _paymentController.Get(_paymentIdException);
                var result = response.As<ObjectResult>();

                result.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
            }
        }

        public class Post : PaymentControllerTest
        {
            [Fact]
            public async Task Correct_results_are_returned_when_post_payment_is_called()
            {
                var response = await _paymentController.Post(_paymentRequest);
                var result = response.As<ObjectResult>().Value;

                result.Should().BeEquivalentTo(_paymentResponse);
            }

            [Fact]
            public async Task Not_exists_is_returned_when_no_payment_is_found()
            {
                var response = await _paymentController.Post(_paymentRequestBadCurrency);
                var result = response.As<ObjectResult>();

                result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            }

            [Fact]
            public async Task Returns_500_response_when_unhandled_exception_occurs()
            {
                var response = await _paymentController.Post(_paymentRequestException);
                var result = response.As<ObjectResult>();

                result.StatusCode.Should().Be(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
