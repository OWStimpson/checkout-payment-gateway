﻿using checkout_payment_gateway_api_tests;
using checkout_payment_gateway_service.Data;
using checkout_payment_gateway_service.Entity;
using checkout_payment_gateway_service.Helpers;
using checkout_payment_gateway_service.Service;
using checkout_payment_gateway_service.ServiceModels;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace checkout_payment_gateway_service_tests
{
    public class PaymentServiceTests : TestBase
    {
        public abstract class PaymentServiceTest : TestBase
        {
            protected readonly PaymentService _paymentService;
            protected readonly Mock<ILogger<PaymentService>> _loggerMock = new Mock<ILogger<PaymentService>>();
            protected readonly Mock<IRepository> _repositoryMock = new Mock<IRepository>();
            protected readonly Mock<IBankHttpClient> _bankHttpClientMock = new Mock<IBankHttpClient>();
            protected readonly Mock<ICardMasker> _cardMaskerMock = new Mock<ICardMasker>();
            protected readonly int _paymentIdExists;
            protected readonly int _paymentIdException;
            protected readonly string _cardNumber;
            protected readonly string _cardNumberMasked;
            protected readonly Payment _payment;
            protected readonly Payment _paymentApproved;
            protected readonly Payment _paymentException;
            protected readonly Payment _paymentApprovedMasked;
            protected readonly BankPaymentRequest _bankPaymentRequest;
            protected readonly BankPaymentResponse _bankPaymentResponse;

            protected PaymentServiceTest()
            {
                _paymentService = new PaymentService(_loggerMock.Object,
                                                     _mapper,
                                                     _repositoryMock.Object,
                                                     _bankHttpClientMock.Object,
                                                     _cardMaskerMock.Object);

                #region Setup

                _paymentIdExists = 1;

                _paymentIdException = 2;

                _cardNumber = "1234123412341234";

                _cardNumberMasked = "************1234";

                _payment = new Payment()
                {
                    CardNumber = _cardNumber,
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Some Ref"
                };

                _paymentException = new Payment()
                {
                    CardNumber = _cardNumber,
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Exception"
                };

                _paymentApproved = new Payment()
                {
                    Id = _paymentIdExists,
                    CardNumber = _cardNumber,
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Status = "Approved",
                    Reference = "Some Ref",
                    UniqueBankReference = Guid.Parse("cdfc7d95-aaad-4030-9099-a6626a926837")
                };

                _paymentApprovedMasked = new Payment()
                {
                    Id = _paymentIdExists,
                    CardNumber = _cardNumberMasked,
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Status = "Approved",
                    Reference = "Some Ref",
                    UniqueBankReference = Guid.Parse("cdfc7d95-aaad-4030-9099-a6626a926837")
                };

                _bankPaymentRequest = new BankPaymentRequest()
                {
                    CardNumber = _cardNumber,
                    CardHolderName = "Mr O Stimpson",
                    Cvv = 999,
                    Currency = "GBP",
                    ExpiryDate = "11/20",
                    Amount = 100,
                    Reference = "Some Ref"
                };

                _bankPaymentResponse = new BankPaymentResponse()
                {
                    PaymentStatus = "Approved",
                    UniqueBankReference = Guid.Parse("cdfc7d95-aaad-4030-9099-a6626a926837")
                };

                #endregion
                #region Mocks

                _repositoryMock.Setup(x => x.GetByIdAsync<Payment>(It.Is<int>(s => s.Equals(_paymentIdExists))))
                    .Returns(Task.FromResult(_paymentApproved));

                _repositoryMock.Setup(x => x.GetByIdAsync<Payment>(It.Is<int>(s => s.Equals(_paymentIdException))))
                    .Throws(new Exception());

                _repositoryMock.Setup(x => x.AddAsync<Payment>(It.Is<Payment>
                    (s => s.Reference == _payment.Reference)))
                    .Returns(Task.FromResult(_paymentApproved));

                _cardMaskerMock.Setup(x => x.Mask(It.Is<string>(s => s.Equals(_cardNumber))))
                    .Returns(_cardNumberMasked);

                _bankHttpClientMock.Setup(x => x.PaymentRequest(It.Is<BankPaymentRequest>
                    (s => s.Reference == _bankPaymentRequest.Reference)))
                    .Returns(Task.FromResult(_bankPaymentResponse));

                _bankHttpClientMock.Setup(x => x.PaymentRequest(It.Is<BankPaymentRequest>
                    (s => s.Reference == _paymentException.Reference)))
                    .Throws(new Exception());

                #endregion
            }
        }

        public class GetPayment : PaymentServiceTest
        {
            [Fact]
            public async Task Correct_results_are_returned_when_get_payment_is_called()
            {
                var result = await _paymentService.GetPayment(_paymentIdExists);

                result.Should().BeEquivalentTo(_paymentApprovedMasked);
            }

            [Fact]
            public async Task Internal_server_error_is_returned_when_unhandelled_exception_is_thrown()
            {
                Func<Task<Payment>> func = async () => await _paymentService.GetPayment(_paymentIdException);

                func.Should().Throw<Exception>();
            }
        }

        public class PaymentRequest : PaymentServiceTest
        {
            [Fact]
            public async Task Correct_results_are_returned_when_payment_request_is_called()
            {
                var result = await _paymentService.PaymentRequest(_payment);

                result.Should().BeEquivalentTo(_paymentApproved);
            }

            [Fact]
            public async Task Internal_server_error_is_returned_when_unhandelled_exception_is_thrown()
            {
                Func<Task<Payment>> func = async () => await _paymentService.PaymentRequest(_paymentException);

                func.Should().Throw<Exception>();
            }
        }
    }
}
