﻿using AutoMapper;
using checkout_payment_gateway_api.Mapping;

namespace checkout_payment_gateway_api_tests
{
    public abstract class TestBase
    {
        protected static readonly IMapper _mapper;

        static TestBase()
        {
            var mappingConfig = new MapperConfiguration(mc => mc.AddProfile(new AutoMapperProfile()));
            _mapper = mappingConfig.CreateMapper();
        }
    }
}